/**
 * @file	citroenBerlingoTU3JP.h
 *
 * @date Apr 15, 2014
 * @author Andrey Belomutskiy, (c) 2012-2014
 */

#ifndef CITROENBERLINGOTU3JP_H_
#define CITROENBERLINGOTU3JP_H_

#include "engine_configuration.h"

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

void setCitroenBerlingoTU3JPConfiguration(engine_configuration_s *engineConfiguration, board_configuration_s *boardConfiguration);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* CITROENBERLINGOTU3JP_H_ */
