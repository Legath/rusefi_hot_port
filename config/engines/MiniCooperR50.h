/*
 * @file MiniCooperR50.h
 *
 * @date Apr 9, 2014
 * @author Andrey Belomutskiy, (c) 2012-2014
 */

#ifndef MINICOOPERR50_H_
#define MINICOOPERR50_H_

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

#include "main.h"
#include "engine_configuration.h"

void setMiniCooperR50(engine_configuration_s *engineConfiguration, board_configuration_s *boardConfiguration);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* MINICOOPERR50_H_ */
