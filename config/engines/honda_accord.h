/**
 * @file	honda_accord.h
 *
 * @date Jan 12, 2014
 * @author Andrey Belomutskiy, (c) 2012-2014
 */

#ifndef HONDA_ACCORD_H_
#define HONDA_ACCORD_H_

#include "engine_configuration.h"

void setHondaAccordConfiguration(engine_configuration_s *engineConfiguration);

#endif /* HONDA_ACCORD_H_ */
