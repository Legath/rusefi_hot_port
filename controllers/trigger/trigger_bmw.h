/**
 * @file	trigger_bmw.h
 *
 * @date May 11, 2014
 * @author Andrey Belomutskiy, (c) 2012-2014
 */
#ifndef TRIGGER_BMW_H_
#define TRIGGER_BMW_H_

#include "engine_configuration.h"
#include "ec2.h"

void configureMiniCooperTriggerShape(engine_configuration_s *engineConfiguration,
		engine_configuration2_s *engineConfiguration2);

#endif /* TRIGGER_BMW_H_ */
