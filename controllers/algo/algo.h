/*
 * @file	algo.h
 *
 * @date Mar 2, 2014
 * @author Andrey Belomutskiy, (c) 2012-2014
 */

#ifndef ALGO_H_
#define ALGO_H_

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

void initAlgo(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* ALGO_H_ */
