/*
 * hardware.h
 *
 * @date May 27, 2013
 * @author Andrey Belomutskiy, (c) 2012-2014
 */

#ifndef HARDWARE_H_
#define HARDWARE_H_

#include "main.h"

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

void initHardware(Logging *logging);
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* HARDWARE_H_ */
