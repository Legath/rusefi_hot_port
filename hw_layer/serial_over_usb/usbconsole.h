/**
 * @file    usbconsole.h
 *
 * @date Oct 14, 2013
 * @author Andrey Belomutskiy, (c) 2012-2014
 */

#ifndef USBCONSOLE_H_
#define USBCONSOLE_H_

void usb_serial_start(void);
int is_usb_serial_ready(void);

#endif /* USBCONSOLE_H_ */
